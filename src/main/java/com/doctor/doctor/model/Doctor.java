package com.doctor.doctor.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="doctor")
public class Doctor{

    @Id
    @Column(name = "doctor_id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long doctorId;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "doctor_name")
    private String doctorName;

    @Column(name = "phone_number")
    private int phoneNumber;

    @Column(name = "national_id")
    private String nationalID;

@JsonManagedReference
    @OneToMany(mappedBy = "doctor",cascade = CascadeType.ALL)
    @JoinColumn(name = "doctor_id",insertable = false,updatable = false)
    private List<Appointment> appointments;

    public Doctor( int phoneNumber, String nationalID) {
        this.phoneNumber = phoneNumber;
        this.nationalID = nationalID;
    }
}
