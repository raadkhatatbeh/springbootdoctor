package com.doctor.doctor.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.Column;
import jakarta.persistence.*;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="patient")
public class Patient{

    @Id
    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "patient_name")
    private String patientName;

    @Column(name = "phone_number")
    private int phoneNumber;

    @Column(name = "age")
    private int age;

    @Column(name = "gender")
    private Character gender;

    @JsonManagedReference
    @OneToMany(mappedBy = "patient",cascade = CascadeType.ALL)
    @JoinColumn(name = "patient_id",insertable = false,updatable = false)
    private List<Appointment> appointments;

    public Patient( int phoneNumber, int age, Character gender) {
        this.phoneNumber = phoneNumber;
        this.age = age;
        this.gender = gender;
    }
}

