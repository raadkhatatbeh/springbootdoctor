package com.doctor.doctor.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Table(name="appointment")
public class Appointment {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "appointment_id")
    private Long id;

   // @JsonFormat( pattern = "dd/MM/yyyy")
    @Column(name = "date")
    private LocalDate date;

   // @JsonFormat( pattern = "HH:mm")
    @Column(name = "time")
    private LocalTime time;


    @Column(name = "status", columnDefinition = "integer default 1")
    private int status;

        @JsonBackReference
        @ManyToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "patient_id",insertable = false,updatable = false)
        private Patient patient;

        @JsonBackReference
        @ManyToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "doctor_id",insertable = false,updatable = false)
        private Doctor doctor;

    public Appointment(LocalDate date, LocalTime time, Patient patient, Doctor doctor) {
        this.date = date;
        this.time = time;
        this.status = 1;
        this.patient = patient;
        this.doctor = doctor;
    }

    public Appointment(Long appointmentId, int status) {
        this.id = appointmentId;
        this.status = status;
    }
}