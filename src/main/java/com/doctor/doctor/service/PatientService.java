package com.doctor.doctor.service;

import com.doctor.doctor.model.Appointment;
import com.doctor.doctor.model.Doctor;
import com.doctor.doctor.model.Patient;
import com.doctor.doctor.repository.DoctorRepository;
import com.doctor.doctor.repository.PatientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private AppointmentService appointmentService;

    public boolean createPatient(Patient patient){
        Patient created = patientRepository.save(patient);
        if(created == null){
            return false;
        }else{
            return true;
        }
    }


    public boolean updatePatient(Patient patient){
        Optional<Patient> fetch = patientRepository.findById(patient.getPatientId());
        if(fetch.isEmpty()){
            return false;
        }else{
            Patient toUpdate = fetch.get();
            toUpdate.setPassword(patient.getPassword());
            toUpdate.setPatientName(patient.getPatientName());
            toUpdate.setPhoneNumber(patient.getPhoneNumber());
            toUpdate.setAge(patient.getAge());
            toUpdate.setGender(patient.getGender());
            patientRepository.saveAndFlush(toUpdate);
            return true;
        }
    }


    public List<Doctor> getAllDoctors(){
        return doctorRepository.findAll();
    }


    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }


    public Patient findPatientById(Long patientId) {
        return patientRepository.findById(patientId).get();
    }


    public boolean createAppointment(Appointment appointment){
        boolean checkBookTime = appointmentService.checkBookedTime(appointment.getTime());
        if(checkBookTime){
            return false;
        }else{
            return appointmentService.createAppointment(appointment);
        }
    }


    public boolean cancelAppointment(Long appointmentId){
        Appointment appointment = appointmentService.findAppointmentById(appointmentId);
        return appointmentService.deleteAppointment(appointment);
    }




}
