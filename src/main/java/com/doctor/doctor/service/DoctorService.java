package com.doctor.doctor.service;


import com.doctor.doctor.dto.AppointmentDto;
import com.doctor.doctor.model.Appointment;
import com.doctor.doctor.model.Doctor;
import com.doctor.doctor.model.Patient;
import com.doctor.doctor.repository.DoctorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private PatientService patientService;

    @Autowired
    private AppointmentService appointmentService;




    public boolean createDoctor(Doctor doctor){
        Doctor created = doctorRepository.save(doctor);
        if(created == null){
            return false;
        }else{
            return true;
        }
    }


    public boolean updateDoctor(Doctor doctor){
        Optional<Doctor> fetch = doctorRepository.findById(doctor.getDoctorId());
        if(fetch.isEmpty()){
            return false;
        }else{
            doctorRepository.saveAndFlush(doctor);
            return true;
        }
    }


    public List<LocalTime> getAllAvailableAppointmentTime(){
        return appointmentService.getAllAvailableAppointments().stream().map(Appointment::getTime).collect(Collectors.toList());
    }


    public List<Appointment> getAllBookedAppointments(){

        return appointmentService.getAllBookedAppointments();
    }

    public boolean createAppointment(Appointment appointment){
        boolean checkBookTime = appointmentService.checkBookedTime(appointment.getTime());
        if(checkBookTime){
            return false;
        }else{
            return appointmentService.createAppointment(appointment);
        }
    }


    public boolean cancelAppointment(Long appointmentId){
        Appointment appointment = appointmentService.findAppointmentById(appointmentId);
        return appointmentService.deleteAppointment(appointment);
    }


    public boolean updateAppointmentStatus(Appointment appointment){
        return appointmentService.updateAppointment(appointment);
    }


    public int totalPatientVisitedDoctor(AppointmentDto appointmentDto){
        return appointmentService.totalVisitByPatients(appointmentDto.getPatient(), appointmentDto.getDoctor());
    }

    public List<Patient> getAllPatients(){
        return patientService.getAllPatients();
    }


    public Patient getPatient(Long patientId){
        return patientService.findPatientById(patientId);
    }


}
